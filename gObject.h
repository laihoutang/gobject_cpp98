/*
 * Copyright (C) 2020  明心  <imleizhang@qq.com>
 * All rights reserved.
 *
 * This program is an open-source software; and it is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * This program is not a free software; so you can not redistribute it(include
 * binary form and source code form) without my authorization. And if you
 * need use it for any commercial purpose, you should first get commercial
 * authorization from me, otherwise there will be your's legal&credit risks.
 *
 */

#ifndef GOBJECT_H
#define GOBJECT_H

#include <stddef.h>
#include <pthread.h>
#include <string.h>  
#include <string>
#include <pthread.h>

#include <cxxabi.h>
#include <stdlib.h>
#include <list>


using namespace std;


class GObjectPrivate;
class GObject;
class GEvent;

#define slots
#define signals public
#define emit

#define SIGNAL_TYPE(SlotFuncType)  list<Slot<SlotFuncType> >
#define SIGNAL_POINTER(SlotFuncType)  SIGNAL_TYPE(SlotFuncType)*
#define SIGNAL_TYPE_ITERATOR(SlotFuncType)  list<Slot<SlotFuncType> >::iterator

typedef void (*T_pnrv)( GObject* );
typedef void (*T_pirv)( GObject*p, int );

/**
 * @def 定义一个函数类型为SlotFuncTypeName，函数名称为signalName的信号。
 *
 * SlotFuncTypeName 信号函数的类型，比如T_pnrv
 * signalName 信号名称
 */
#define DEFINE_SIGNAL(SlotFuncTypeName, signalName) \
    public: SIGNAL_POINTER(SlotFuncTypeName) signalName() { return reinterpret_cast<SIGNAL_POINTER(SlotFuncTypeName)>(&m_##signalName); } \
    private: SIGNAL_TYPE(void*) m_##signalName; \
    private: SlotFuncTypeName m_##signalName##Type;

#define DEFINE_SIGNAL_EX(SlotFuncType, SlotFuncTypeName, signalName) \
    public: typedef SlotFuncType; \
    public: SIGNAL_POINTER(SlotFuncTypeName) signalName() { return reinterpret_cast<SIGNAL_POINTER(SlotFuncTypeName)>(&m_##signalName); } \
    private: SIGNAL_TYPE(void*) m_##signalName; \
    private: SlotFuncTypeName m_##signalName##Type;

#define EMIT_SIGNAL_EX(SlotFuncType,signalName, ...) \
    m_##signalName##Type = (SlotFuncType)NULL; \
    for(SIGNAL_TYPE(void*)::iterator it = m_##signalName.begin(); it != m_##signalName.end(); ++it) \
    { \
        if(NULL != it->m_receiver) \
            (*(SlotFuncType)(it->m_slot))(it->m_receiver, __VA_ARGS__); \
    }

#define EMIT_SIGNAL(SlotFuncType,signalName) \
    m_##signalName##Type = (SlotFuncType)NULL; \
    for(SIGNAL_TYPE(void*)::iterator it = m_##signalName.begin(); it != m_##signalName.end(); ++it) \
    { \
        if(NULL != it->m_receiver) \
            (*(SlotFuncType)(it->m_slot))(it->m_receiver); \
    }

#define SET_CLASS_NAME(any_type) \
public: \
    virtual const char *className() const \
    { \
        static string s_name; \
        char* name = abi::__cxa_demangle(typeid(any_type).name(), NULL, NULL, NULL); \
        s_name = name; \
        free(name); \
        return s_name.c_str(); \
    }

template<class SlotFuncType>
struct Slot
{
    Slot( GObject* receiver, SlotFuncType slot)
    :m_receiver(receiver),
    m_slot(slot)
    {
    }
    
    bool operator==(const Slot& other)
    {
        return other.m_receiver == m_receiver && other.m_slot == m_slot;
    }

    GObject* m_receiver;
    SlotFuncType m_slot;
};


class GObject
{
    SET_CLASS_NAME(GObject)
    DEFINE_SIGNAL(T_pnrv, sigDestroyed);
    
private:
    GObjectPrivate *m_priv;

public:
    explicit GObject ( GObject *parent=NULL,  const char *name=NULL );
    GObject( const GObject & src);
    GObject & operator=(const GObject & src);
    virtual ~GObject();
 
    /**
     * @brief 将信号连接到槽。\n
     * SlotFuncType是槽函数的函数类型名称。
     *
     * @param sender 指向发射者的指针
     * @param signal 指向信号的指针。
     * @param receiver 指向接收者的指针
     * @param slot 指向槽函数的指针
     *
     * @return 0代表成功；非0代表失败
     */
    template<class SlotFuncType>
    static int  connect(GObject* sender, SIGNAL_POINTER(SlotFuncType) signal, GObject* receiver, SlotFuncType slot);
    
    /**
     * @brief 将信号和槽断开连接。\n
     * SlotFuncType是槽函数的函数类型名称。
     *
     * @param sender 指向发射者的指针
     * @param signal 指向信号的指针。
     * @param receiver 指向接收者的指针
     * @param slot 指向槽函数的指针
     *
     * @return 0代表成功；非0代表失败
     */
    template<class SlotFuncType>
    static int  disconnect(GObject* sender, SIGNAL_POINTER(SlotFuncType) signal, GObject* receiver, SlotFuncType slot);

    const char *name() const;
    GObject *parent() const;
    virtual bool event(GEvent*);
    pthread_t  tid();
    
private:
    static int  privConnect(GObject* sender, SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot);
    static int  privDisconnect(GObject* sender, SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot);
    void saveSender(SIGNAL_POINTER(void*) signal);
    void deleteSender(SIGNAL_POINTER(void*) signal);
    void disconnectFromAllSignal();
};

template<class SlotFuncType>
int   GObject::connect(GObject* sender, SIGNAL_POINTER(SlotFuncType) signal, GObject* receiver, SlotFuncType slot)
{
    int ret = privConnect(sender, reinterpret_cast<SIGNAL_POINTER(void*)>(signal), receiver, (void*)slot);
    return ret;
}

template<class SlotFuncType>
int   GObject::disconnect(GObject* sender, SIGNAL_POINTER(SlotFuncType) signal, GObject* receiver, SlotFuncType slot)
{
    int ret = privDisconnect(sender, reinterpret_cast<SIGNAL_POINTER(void*)>(signal), receiver, (void*)slot);
    return ret;
}

#endif // GOBJECT_H
