/*
 * Copyright (C) 2020  明心  <imleizhang@qq.com>
 * All rights reserved.
 *
 * This program is an open-source software; and it is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * This program is not a free software; so you can not redistribute it(include
 * binary form and source code form) without my authorization. And if you
 * need use it for any commercial purpose, you should first get commercial
 * authorization from me, otherwise there will be your's legal&credit risks.
 *
 */

#include "gObject.h"
#include <stdio.h>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

#define WARNING printf

class GObjectPrivate
{
public:
    GObjectPrivate ( GObject *p, const char* nm ) :m_parent ( p ), strName ( nm ) 
    {
        tid = pthread_self();
    }

    GObject *m_parent;
    string   strName;
    pthread_t  tid;
    list<SIGNAL_POINTER(void*) >  senderLst;
};

GObject::GObject ( GObject *p,  const char *n )
    :m_priv ( new GObjectPrivate ( p, NULL==n?"":n ) )
{

}

GObject::~GObject()
{
    disconnectFromAllSignal();

    EMIT_SIGNAL(T_pnrv, sigDestroyed);

    delete m_priv;
}

static void saveSlot2Signal(SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot)
{
    SIGNAL_TYPE_ITERATOR(void*)  it = std::find(signal->begin(), signal->end(), Slot<void*>(receiver, (void*)slot) );
    if(it != signal->end() )
    {
        return ;
    }

    signal->push_back( Slot<void*>(receiver, (void*)slot) );
}

void GObject::saveSender(SIGNAL_POINTER(void*) signal)
{
    list<SIGNAL_POINTER(void*) >::iterator it = std::find(m_priv->senderLst.begin(), m_priv->senderLst.end(), signal);
    if(it != m_priv->senderLst.end() )
    {
        return ;
    }

    m_priv->senderLst.push_back(signal);
}

void GObject::deleteSender(SIGNAL_POINTER(void*) signal)
{
    m_priv->senderLst.remove(signal);
}

static void deleteSlotFromSignal(SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot)
{
    signal->remove(Slot<void*>(receiver, (void*)slot) );
}

class Receiver_Is
{
public:
    GObject* receiver;

    bool operator( ) ( Slot<void*> &obj1 )
    {
        return obj1.m_receiver == receiver;
    }

    Receiver_Is(GObject* r)
        :receiver(r)
    {

    }
};

static void deleteReceiverFromSignal(SIGNAL_POINTER(void*) signal, GObject* receiver)
{
    signal->remove_if( Receiver_Is(receiver) );
}

int GObject::privConnect(GObject* sender, SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot)
{
    if ( sender == 0 || receiver == 0 || signal == 0 || slot == 0 )
    {
        WARNING ( "can not connect %s::%p to %s::%p\n",
                  sender ? sender->className() : "(null)",
                  signal,
                  receiver ? receiver->className() : "(null)",
                  slot );
        return -1;
    }

    saveSlot2Signal(signal, receiver, slot );
    
    receiver->saveSender(signal);
    
    return 0;
}

void GObject::disconnectFromAllSignal()
{
    list<SIGNAL_POINTER(void*) >::iterator it = m_priv->senderLst.begin();
    while(it != m_priv->senderLst.end() )
    {
        SIGNAL_POINTER(void*) signal=*it;
        deleteReceiverFromSignal(signal, this);
        ++it;
    }
}

int GObject::privDisconnect(GObject* sender, SIGNAL_POINTER(void*) signal, GObject* receiver, void* slot)
{
    if ( sender == 0 || receiver == 0 || signal == 0 || slot == 0 )
    {
        WARNING ( "can not disconnect sender %s::%p from receiver %s::%p\n",
                  sender ? sender->className() : "(null)",
                  signal,
                  receiver ? receiver->className() : "(null)",
                  slot );
        return -1;
    }
    
    deleteSlotFromSignal(signal, receiver, slot );
    
    receiver->deleteSender(signal);
    
    return 0;
}

const char* GObject::name() const
{
    return  m_priv->strName.c_str();
}

GObject* GObject::parent() const
{
    return m_priv->m_parent;
}

GObject& GObject::operator= ( const GObject&  )
{
    return *this;
}

GObject::GObject ( const GObject&  )
    :m_priv ( new GObjectPrivate ( NULL, "" ) )
{
}

bool GObject::event(GEvent* )
{
    return false;
}

pthread_t GObject::tid()
{
    return m_priv->tid;
}
